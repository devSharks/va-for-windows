﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Numerics;
using System.Reflection;

namespace pj050517
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var header = new WavHeader();
            // Размер заголовка
            var headerSize = Marshal.SizeOf(header);

            var fileStream = new FileStream("7.wav", FileMode.Open, FileAccess.Read);
            var buffer = new byte[headerSize];
            fileStream.Read(buffer, 0, headerSize);

            // Чтобы не считывать каждое значение заголовка по отдельности,
            // воспользуемся выделением unmanaged блока памяти
            var headerPtr = Marshal.AllocHGlobal(headerSize);
            // Копируем считанные байты из файла в выделенный блок памяти
            Marshal.Copy(buffer, 0, headerPtr, headerSize);
            // Преобразовываем указатель на блок памяти к нашей структуре
            Marshal.PtrToStructure(headerPtr, header);

            // Выводим полученные данные
            label1.Text = "Sample rate: "+ header.SampleRate;
            label2.Text = "Channels: "+ header.NumChannels;
            label3.Text = "Bits per sample: "+ header.BitsPerSample;

            // Посчитаем длительность воспроизведения в секундах
            var durationSeconds = 1.0 * header.Subchunk2Size / (header.BitsPerSample / 8.0) / header.NumChannels / header.SampleRate;
            var durationMinutes = (int)Math.Floor(durationSeconds / 60);
            durationSeconds = durationSeconds - (durationMinutes * 60);
            label4.Text = "Duration:"+ durationMinutes + ":" + durationSeconds;

            // Освобождаем выделенный блок памяти
            Marshal.FreeHGlobal(headerPtr);
        
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            Wav wav = new Wav();
            wav.parce("666.wav");
            textBox1.ScrollBars = ScrollBars.Vertical;
            // Allow the RETURN key to be entered in the TextBox control.
            textBox1.AcceptsReturn = true;
            // Allow the TAB key to be entered in the TextBox control.
            textBox1.AcceptsTab = true;
            // Set WordWrap to true to allow text to wrap to the next line.
            textBox1.WordWrap = true;
            // Set the default text of the control.
            // for(int i = 0; i<300;i++)
            // {
            //textBox1.Text += wav.fft[i].ToString()+"\n";
            //}
            
            for (int j = 0; j < wav.fft.Count; j++)
            {
                int last = 0;
                int count = 0;
                int frame = 10;
                double tmp = 0;
                StreamWriter sw = new StreamWriter("666-" + j.ToString() + ".txt");
                for (int i = 0; i < wav.fft[j].Length / 2; i++)
                {
                    double amp = Math.Sqrt(wav.fft[j][i].Real * wav.fft[j][i].Real + wav.fft[j][i].Imaginary * wav.fft[j][i].Imaginary);
                    int f = i * 44100 / (wav.N * 2);
                    if (f < -1) break;
                    if (f == last)
                    {
                        count++;
                        tmp += amp;
                    }
                    else
                    {
                        sw.WriteLine(tmp / count);
                        count = 1;
                        tmp = amp;
                        last = f;
                    }
                }
                sw.Close();
            }

            for (int j = 0; j < wav.fft.Count; j++)
            {
                int frame = wav.fft[j].Length / 8;
                StreamWriter sw = new StreamWriter("cmp-" + j.ToString() + ".txt");
                Double[] tmp = new Double[4];
                for (int i = 0; i < 4; i++)
                {
                    tmp[i] = wav.maxOfFrame(wav.fft[j].ToArray(), i * frame, frame);
                    //sw.WriteLine(tmp[i]);
                }
                tmp = wav.normalize(tmp);
                for (int i = 0; i < 4; i++)
                    sw.WriteLine(tmp[i]);
                wav.range.Add(tmp);
                tmp = null;
                sw.Close();
            }

            wav = null;
            GC.Collect(GC.MaxGeneration);

            /*for (int j = 0; j < wav.fft.Count; j++)
            {
                int frame = 50;
                StreamWriter sw = new StreamWriter("comp-" + j.ToString() + ".txt");
                for (int i = 0; i < wav.fft[j].Length-frame-1 / 2; i+=frame)
                {
                    double amp = wav.maxOfFrame(wav.fft[j], i, frame);
                    sw.WriteLine(amp);
                }
            }*/
            /* int last = -1;
             int count = 0;
             double tmp = 0;
             StreamWriter sw = new StreamWriter("20.txt");
             for (int i = 0; i < wav.fft.Length / 2; i++)
             {
                 double amp = Math.Sqrt(wav.fft[i].Real * wav.fft[i].Real + wav.fft[i].Imaginary * wav.fft[i].Imaginary);
                 int f = i * 44100 / (wav.N * 2);
                 if (f < -1) break;
                 if (f == last)
                 {
                     count++;
                     tmp += amp;
                 }
                 else
                 {
                     sw.WriteLine(tmp / count);
                     count = 1;
                     tmp = amp;
                     last = f;
                 }
             }
                 /*if (f != last)
                 {

                     // sw.Write(amp + " - ");
                     //sw.WriteLine(f);
                     //sw.WriteLine(amp);
                     last = f;
                 }
        }
        // textBox1.Text = wav.fft.Length.ToString();*/
        }
    }
}
